<?php

declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Events\ChatEvent;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Contracts\View\View as ViewContract;

/**
 * Class ChatController
 *
 * @package App\Http\Controllers
 */
class ChatController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(): ViewContract
    {
        return View::make('chat');
    }

    /**
     * @param Request $request
     *
     * @return void
     */
    public function sendMessage(Request $request): void
    {
        $user = User::find(Auth::id());
        event(new ChatEvent($request->message, $user));
    }
}
