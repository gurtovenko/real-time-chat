<?php

declare(strict_types = 1);

namespace App\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

/**
 * Class DevelopmentServiceProvider
 *
 * @package App\Providers
 */
class DevelopmentServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $providers = [
        \Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class,
    ];

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        if (!Config::get('app.debug')) {
            return;
        }

        foreach ($this->providers as $provider) {
            $this->app->register($provider);
        }
    }
}
