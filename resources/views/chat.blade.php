@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-4 frame">
            <ul class="chat-room">
                <chat-messages
                    v-for="message, index in chat.messages"
                    :key="index"
                    position="left"
                >
                    @{{ message }}
                </chat-messages>
            </ul>
            <div>
                <div class="chat-text-field">
                    <input class="chat-input"
                           placeholder="Type a message"
                           v-model="message"
                           @keyup.enter="pushMessage"
                    >
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
